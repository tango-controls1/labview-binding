@ECHO off

::------------------------------------------------------------------------
:: SETUP THE RUNTIME ENV.
::------------------------------------------------------------------------
REM set PATH=C:\Program Files\tango\bindings\labview-binding-gitlab\visrc;%PATH%
REM set PATH=C:\Program Files\tango\win64\lib\vc12_dll;%PATH%
REM set PATH=C:\Program Files (x86)\National Instruments\LabVIEW 2020;%PATH%

REM start LabVIEW.exe

set LVB_PATH=C:\Users\AlexK\work\Tango\HIJ-Tango-Controls@GitLab.com\labview-binding
set PATH=%LVB_PATH%\runtime;%PATH%
set PATH=C:\Program Files (x86)\National Instruments\LabVIEW 2020;%PATH%

start LabVIEW.exe